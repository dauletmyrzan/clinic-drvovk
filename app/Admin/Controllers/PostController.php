<?php

namespace App\Admin\Controllers;

use App\Models\Post;
use App\Models\User;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PostController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Статьи';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Post());

        $grid->column('id', __('Id'));
        $grid->column('title', __('Заголовок'));
        $grid->column('active', __('Активно'))->using([
            1 => 'Да',
            0 => 'Нет',
        ]);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Post::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Заголовок'));
        $show->field('slug', __('Ссылка'));
        $show->field('content', __('Содержимое'));
        $show->field('user_id', __('User id'));
        $show->field('img', __('Img'));
        $show->field('created_at', __('Дата создания'));
        $show->field('updated_at', __('Дата изменения'));
        $show->field('active', __('Активно'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Post());

        $form->text('title', __('Заголовок'));
        $form->summernote('content', __('Содержимое'));
        $form->image('img', __('Изображение'));
        $form->switch('active', __('Активно'))->default(1);

        return $form;
    }
}
