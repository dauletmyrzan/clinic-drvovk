<?php

namespace App\Admin\Controllers;

use App\Models\Service;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ServiceController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Услуги';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Service());

        $grid->column('id', __('Id'));
        $grid->column('title', __('Название'));
        $grid->column('slug', __('Ссылка'))->display(function ($slug) {
            return "<a href='" . route('service', $slug) . "' target='_blank'>Открыть на сайте</a>";
        });
        $grid->column('created_at', __('Дата создания'))->display(function ($date) {
            return $date ? date('Y-m-d H:i', strtotime($date)) : '';
        });;
        $grid->column('updated_at', __('Дата изменения'))->display(function ($date) {
            return $date ? date('Y-m-d H:i', strtotime($date)) : '';
        });;

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Service::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Название'));
        $show->field('slug', __('Ссылка'));
        $show->field('content', __('Содержимое'));
        $show->field('created_at', __('Дата создания'));
        $show->field('updated_at', __('Дата изменения'));
        $show->field('img', __('Картинка'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Service());

        $form->text('title', __('Заголовок'));
        $form->text('slug', __('Ссылка'));
        $form->summernote('content', __('Содержимое'));
        $form->image('img', __('Img'));

        return $form;
    }
}
