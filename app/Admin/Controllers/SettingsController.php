<?php

namespace App\Admin\Controllers;

use App\Models\Settings;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SettingsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Настройки';

    protected $description = 'Здесь вы можете редактировать номера телефонов, WhatsApp, почту, адрес и слоган компании.';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Settings());

        $grid->column('key', __('Ключ'));
        $grid->column('value', __('Значение'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Settings::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('key', __('Ключ'));
        $show->field('value', __('Значение'));
        $show->field('created_at', __('Дата создания'));
        $show->field('updated_at', __('Дата изменения'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Settings());

        $form->text('key', __('Ключ'));
        $form->text('value', __('Значение'));

        return $form;
    }
}
