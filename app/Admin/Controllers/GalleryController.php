<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use \App\Models\Gallery;

class GalleryController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Галерея';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Gallery());

        $grid->model()->orderBy('sort_number');
        $grid->column('filename')->image('', '150', '150');
        $grid->column('sort_number', 'Номер сортировки')->sortable()->width(200);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Gallery::findOrFail($id));

        $show->field('filename');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form         = new Form(new Gallery());
        $galleryCount = Gallery::query()->count();

        $form->file('filename', 'Файл')->required()->dir('images/gallery');
        $form->number('sort_number', 'Порядок сортировки')->required()->default($galleryCount + 1);

        return $form;
    }
}
