<?php

namespace App\Admin\Controllers;

use App\Models\Team;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TeamController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Команда';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Team());

        $grid->column('id', __('Id'));
        $grid->column('name', __('ФИО'));
        $grid->column('title', __('Должность'));
        $grid->column('sort_number', 'Номер сортировки')->sortable();
        $grid->column('active', __('Активен'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Team::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('ФИО'));
        $show->field('title', __('Должность'));
        $show->field('sort_number', 'Номер сортировки');
        $show->field('img', __('Фото'));
        $show->field('active', __('Активен'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Team());

        $form->text('name', __('ФИО'));
        $form->text('title', __('Должность'));
//        $form->textarea('bio', __('Bio'));
        $form->number('sort_number', 'Номер сортировки')->help('Каким номером будет выходить участник на сайте.');
        $form->image('img', __('Фото'));
        $form->switch('active', __('Активен'))->default(1);

        return $form;
    }
}
