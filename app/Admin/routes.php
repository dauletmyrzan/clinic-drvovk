<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', function () {
        return redirect('/admin/posts');
    })->name('home');

    $router->resource('posts', PostController::class);

    $router->resource('settings', SettingsController::class);

    $router->resource('services', ServiceController::class);

    $router->resource('team', TeamController::class);

    $router->resource('gallery', GalleryController::class);

});
