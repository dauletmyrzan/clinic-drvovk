<?php

namespace App\Providers;

use App\Models\Service;
use App\Models\Settings;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $services = Service::all();
            $view->with('services', $services);
        });

        Blade::directive('footer_desc', function () {
            $setting = Settings::query()->where('key', 'footer_description')->first();

            if (!$setting) {
                return "Центр дентальной имплантации: хирургия, ортопедия, ортодонтия, терапия и детская стоматология.";
            }

            return $setting->value;
        });

        Blade::directive('address', function () {
            $setting = Settings::query()->where('key', 'address')->first();

            if (!$setting) {
                return "ул. Жамбыла 155 (ЖК Отау), Алматы, Казахстан";
            }

            return $setting->value;
        });

        Blade::directive('whatsapp', function () {
            $setting = Settings::query()->where('key', 'whatsapp')->first();

            if (!$setting) {
                return "77013511146";
            }

            return $setting->value;
        });

        Blade::directive('email', function () {
            $setting = Settings::query()->where('key', 'email')->first();

            if (!$setting) {
                return "info@drvovk.clinic";
            }

            return $setting->value;
        });

        Blade::directive('phone', function () {
            $phone       = Settings::query()->where('key', 'phone')->first();
            $mobilePhone = Settings::query()->where('key', 'mobile_phone')->first();
            $template    = "<a href='tel:%s'>
                                <span style=\"font-family:'Open Sans',sans-serif;\">%s</span>
                            </a><br>
                            <a href='tel:%s'>
                                <span style=\"font-family:'Open Sans',sans-serif;\">%s</span>
                            </a>";

            if (!$phone) {
                return sprintf(
                    $template,
                    '+7 (727) 323-6137',
                    '+7 (727) 323-6137',
                    '+7 (701) 351-1146',
                    '+7 (701) 351-1146'
                );
            }

            return sprintf($template, $phone->value, $phone->value, $mobilePhone->value, $mobilePhone->value);
        });

        Blade::directive('search', function () {
            return '<div class="sidebar-box" style="background-color:#f4f4f4;">
                        <form action="#" class="search-form">
                            <div class="form-group">
                                <span class="icon ion-ios-search"></span>
                                <input type="text" class="form-control" placeholder="Поиск...">
                            </div>
                        </form>
                    </div>';
        });
    }
}
