<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Butschster\Head\Contracts\MetaTags\MetaInterface;
use Butschster\Head\Facades\Meta;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $posts = Post::query()->where('active', true)->orderBy('created_at', 'desc')->paginate(10);

        return view('blog.index', compact(['posts']));
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request        $request
     * @return \Illuminate\Contracts\View\View
     */
    public function show(Request $request)
    {
        $post = Post::query()->where('slug', $request->slug)->first();

        if (!$post) {
            abort(404);
        }

        $og = new \Butschster\Head\Packages\Entities\OpenGraphPackage('og');
        $og->setTitle($post->title);
        $og->setUrl($request->url());
        $og->setType('website');

        Meta::registerPackage($og);

        Meta::prependTitle($post->title);

        return view('blog.show', compact(['post']));
    }
}
