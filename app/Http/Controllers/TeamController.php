<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function index()
    {
        $team     = Team::query()->where('active', true)->orderBy('sort_number')->get();
        $services = Service::all();

        return view('team', compact(['team', 'services']));
    }
}
