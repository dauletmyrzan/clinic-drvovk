<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Butschster\Head\Facades\Meta;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function show(Request $request, string $slug)
    {
        $service = Service::query()->where('slug', $slug)->first();

        Meta::prependTitle($service->title);

        return view('services.show', compact(['service']));
    }
}
