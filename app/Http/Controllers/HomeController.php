<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\Post;
use App\Models\Team;
use App\Models\User;
use App\Notifications\RequestSent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{
    public function welcome()
    {
        $team    = Team::query()->where('active', 1)->orderBy('sort_number')->get();
        $gallery = Gallery::query()->orderBy('sort_number')->get();
        $gallery = $gallery->chunk(8);

        return view('welcome', compact(['team', 'gallery']));
    }

    public function about()
    {
        return view('about', compact([]));
    }

    public function contact()
    {
        return view('contact', compact([]));
    }

    public function blog()
    {
        $posts = Post::query()->orderBy('id', 'desc')->paginate(5);

        return view('blog.index', compact(['posts']));
    }

    public function services()
    {
        return view('services.index');
    }

    public function request(Request $request)
    {
        $rule = [
            'g-recaptcha-response' => [new \TimeHunter\LaravelGoogleReCaptchaV3\Validations\GoogleReCaptchaV3ValidationRule('contact_us')]
        ];

        $validator = \Illuminate\Support\Facades\Validator::make($request->toArray(),$rule)->errors();

        if (!empty($validator->toArray())) {
            return response()->json([
                'status' => 'fail',
            ]);
        }

        $data  = [
            'name'  => $request->name,
            'phone' => $request->phone,
        ];
        $users = User::query()->where('notifiable', true)->get();

        Notification::send($users, new RequestSent($data));

        return response()->json([
            'status' => 'ok',
        ]);
    }
}
