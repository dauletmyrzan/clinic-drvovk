<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory, Sluggable;

    protected $fillable = [
        'title', 'content', 'slug',
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function getTTRAttribute()
    {
        $ttr = mb_strlen($this->content)/1500;

        return round($ttr, 0) + 1;
    }
}
