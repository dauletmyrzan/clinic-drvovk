<?php

use App\Http\Controllers\ServiceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    \App\Http\Controllers\HomeController::class,
    'welcome',
]);

Route::get('/team', [
    \App\Http\Controllers\TeamController::class,
    'index',
])->name('team');

Route::get('/about', [
    \App\Http\Controllers\HomeController::class,
    'about',
])->name('about');

Route::get('/contact', [
    \App\Http\Controllers\HomeController::class,
    'contact',
])->name('contact');

Route::get('/blog', [
    \App\Http\Controllers\HomeController::class,
    'blog',
])->name('blog');

Route::get('/services', [
    \App\Http\Controllers\HomeController::class,
    'services',
])->name('services');

Route::get('/services/{slug}', [
    ServiceController::class,
    'show',
])->name('service');

Route::get('/blog/{slug}', [
    \App\Http\Controllers\PostController::class,
    'show',
]);

Route::post('/request', [
    \App\Http\Controllers\HomeController::class,
    'request',
]);

Route::get('/call/{phone}', function ($phone) {
    return redirect('tel:' . $phone);
});

Route::get('/symlink', function () {

});