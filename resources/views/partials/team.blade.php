<div class="carousel-team owl-carousel">
    @foreach ($team as $member)
        <div class="item">
            <a href="javascript:void(0);" class="team text-center">
                <div class="img" style="background-image: url('/storage/{{ $member->img }}');"></div>
                <h2>{{ $member->name }}</h2>
                <span class="position">{{ $member->title }}</span>
            </a>
        </div>
    @endforeach
</div>