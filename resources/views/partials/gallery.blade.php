@foreach ($gallery as $chunk)
    <div class="row no-gutters">
        @foreach ($chunk as $item)
            <div class="col-md-6 col-lg-3 ftco-animate">
                <a href="/storage/{{ $item->filename }}" class="image-popup">
                    <div class="project">
                        <div class="project-img owl-lazy" data-src="/storage/{{ $item->filename }}"></div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endforeach