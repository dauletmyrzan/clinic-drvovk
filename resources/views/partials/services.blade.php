<div class="row no-gutters d-flex">
    @foreach ($services as $key => $service)
        <div class="col-md-6 col-lg-2 @if ($key === 0) offset-md-1 @endif d-flex align-self-stretch ftco-animate">
            <a href="/services/{{ $service->slug }}">
                <div class="media block-6 services d-block text-center">
                    <div class="service-icon" style="background-image:url('/storage/{{ $service->img }}')"></div>
                    <div class="media-body">
                        <h3 class="heading mb-3">{{ $service->title }}</h3>
                    </div>
                </div>
            </a>
        </div>
    @endforeach
</div>