@extends('layouts.app')

@section('content')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('/images/{{ $service->slug }}.jpg');@if($service->slug === 'orthopedics') background-position:center; @endif">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
                <div class="col-md-9 ftco-animate pb-5 text-center">
                    <h2 class="mb-0 bread">{{ $service->title }}</h2>
                    <p class="breadcrumbs">
                        <span class="mr-2">
                            <a href="/">Главная <i class="ion-ios-arrow-round-forward"></i></a>
                        </span> <span class="mr-2"><a href="{{ route('services') }}">Услуги</a> <i class="ion-ios-arrow-forward"></i>
                        </span> <span>{{ $service->title }}</span></p>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ftco-animate">
                    <div class="post-content">
                        {!! $service->content !!}
                    </div>
                </div> <!-- .col-md-8 -->

                <div class="col-lg-4 sidebar ftco-animate">
                    <div class="sidebar-box shadow-sm ftco-animate" style="background-color: #f4f4f4">
                        <h3 class="heading-2">Другие услуги</h3>
                        @foreach ($services as $service)
                            <div class="block-21 mb-2 d-flex">
                                <a class="blog-img mr-4" style="background-image: url('/storage/{{ $service->img }}');"></a>
                                <div class="text">
                                    <h3 class="heading-1"><a href="{{ $service->slug }}">{{ $service->title }}</a></h3>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- .section -->
@endsection