@extends('layouts.app')

@section('content')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/services.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
                <div class="col-md-9 ftco-animate pb-5 text-center">
                    <h2 class="mb-0 bread">Наши услуги</h2>
                    <p class="breadcrumbs"><span class="mr-2"><a href="/">Главная <i class="ion-ios-arrow-round-forward"></i></a></span> <span>Услуги</span></p>
                </div>
            </div>
        </div>
    </section>

    <section class="services-section ftco-section">
        <div class="container" style="max-width:1500px;">
            @include('partials.services', ['services.index' => $services])
        </div>
    </section>

    <section class="ftco-section ftco-booking bg-light">
        <div class="container ftco-relative">
            <div class="row justify-content-center pb-3">
                <div class="col-md-10 heading-section text-center ftco-animate">
                    <span class="subheading">Обратная связь</span>
                    <h2 class="mb-4">Оставьте заявку</h2>
                </div>
            </div>
            <h3 class="vr">Звоните: +7 (701) 351-1146</h3>
            <div class="row justify-content-center">
                <div class="col-md-7 ftco-animate">
                    <form action="#" class="appointment-form">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="appointment_name" placeholder="Имя" name="name">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="phone" placeholder="Номер телефона" name="phone">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!!  GoogleReCaptchaV3::renderField('contact_us', 'contact_us') !!}
                            <input type="submit" value="Оставить заявку" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
{{--6LcIm5kaAAAAADsWbPtAonGNphuYwd35J6hCSc_I--}}
@section('js')
    <script src="{{ asset('js/sweetalert2@10.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function () {
            $(".appointment-form").on('submit', function () {
                $.ajax({
                    url: '/request',
                    type: 'post',
                    data: {
                        'g-recaptcha-response': getReCaptchaV3Response('contact_us'),
                        'name': $("#appointment_name").val(),
                        'phone': $("#phone").val(),
                    },
                    success (response) {
                        if (response['status'] === 'ok') {
                            Swal.fire({
                                title: '',
                                text: 'Ваша заявка принята.',
                                icon: 'success',
                                confirmButtonText: 'Ок'
                            })
                        } else {
                            Swal.fire({
                                title: '',
                                text: 'Ошибка, попробуйте позднее.',
                                icon: 'error',
                                confirmButtonText: 'Ок'
                            })
                        }
                        refreshReCaptchaV3('contact_us', 'contact_us');
                    },
                    error: function (err) {
                        refreshReCaptchaV3('contact_us', 'contact_us');
                    }
                });

                return false;
            });
        });
    </script>
    {!!  GoogleReCaptchaV3::init() !!}
@endsection