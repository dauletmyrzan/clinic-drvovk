<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @meta_tags
    <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed:500,600,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@400;500;600&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/open-iconic-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">

    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('css/aos.css') }}">

    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.timepicker.css') }}">


    <link rel="stylesheet" href="{{ asset('css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css?v=1.1') }}">
    @yield('style')
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        <div class="container">
            <a class="navbar-brand" href="/">Dr. Vovk</a>
            <button class="navbar-toggler px-3" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="oi oi-menu text-white" style="font-size:18pt;"></span>
            </button>

            <div class="collapse navbar-collapse" id="ftco-nav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item {{ Request::is('/') ? 'active' : '' }}"><a href="/" class="nav-link">Главная</a></li>
                    <li class="nav-item {{ Request::is('about') ? 'active' : '' }}"><a href="{{ route('about') }}" class="nav-link">О клинике</a></li>
                    <li class="nav-item {{ Request::is('services') ? 'active' : '' }}"><a href="{{ route('services') }}" class="nav-link">Услуги</a></li>
                    <li class="nav-item {{ Request::is('team') ? 'active' : '' }}"><a href="{{ route('team') }}" class="nav-link">Наша команда</a></li>
                    <li class="nav-item {{ Request::is('blog') ? 'active' : '' }}"><a href="{{ route('blog') }}" class="nav-link">Советы и статьи</a></li>
                    <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}"><a href="{{ route('contact') }}" class="nav-link">Контакты</a></li>
                    <li id="top_phone">@phone</li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- END nav -->

    @yield('content')

    <footer class="ftco-footer ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2 logo">Clinic Dr. Vovk</h2>
                        <p>@footer_desc</p>
                        <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                            <li class="ftco-animate"><a href="https://api.whatsapp.com/send/?phone=77013511146&text&app_absent=0" target="_blank"><span class="icon-whatsapp"></span></a></li>
                            <li class="ftco-animate"><a href="https://www.facebook.com/Vovk.Clinic/" target="_blank"><span class="icon-facebook"></span></a></li>
                            <li class="ftco-animate"><a href="http://instagram.com/clinic_doctor_vovk" target="_blank"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4 ml-md-5">
                        <ul class="list-unstyled">
                            @foreach($services as $service)
                                <li><a href="{{ $service->slug }}" class="py-2 d-block">{{ $service->title }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4">
                        <ul class="list-unstyled">
                            <li><a href="/" class="py-2 d-block">Главная</a></li>
                            <li><a href="{{ route('about') }}" class="py-2 d-block">О клинике</a></li>
                            <li><a href="{{ route('services') }}" class="py-2 d-block">Наши услуги</a></li>
                            <li><a href="{{ route('team') }}" class="py-2 d-block">Наша команда</a></li>
                            <li><a href="{{ route('contact') }}" class="py-2 d-block">Контакты</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Контакты</h2>
                        <div class="block-23 mb-3">
                            <ul>
                                <li><span class="icon icon-map-marker"></span><span class="text">@address</span></li>
                                <li><span class="icon icon-phone"></span><span class="text">@phone</span></li>
                                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">@email</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <hr style="border-color:#444">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                    <p>&copy; Clinic Dr.Vovk <script>document.write(new Date().getFullYear());</script></p>
                </div>
                <div class="col-md-4 text-center text-md-right">
                    <a href="https://jackscode.kz" target="_blank"><img src="{{ asset('images/jc_white.png') }}" width="90" alt="Jackscode.kz"></a>
                </div>
            </div>
        </div>
    </footer>

    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

    <!-- GetButton.io widget -->
    <script type="text/javascript">
        (function () {
            var options = {
                whatsapp: "@whatsapp", // WhatsApp number
                call_to_action: "Отправьте нам сообщение", // Call to action
                position: "right", // Position may be 'right' or 'left'
                pre_filled_message: "Здравствуйте! Хочу проконсультироваться", // WhatsApp pre-filled message
            };
            var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /GetButton.io widget -->

    <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.easing.1.3.js"></script>
    <script src="/js/jquery.waypoints.min.js"></script>
    <script src="/js/jquery.stellar.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/jquery.magnific-popup.min.js"></script>
    <script src="/js/aos.js"></script>
    <script src="/js/jquery.animateNumber.min.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>
    <script src="/js/jquery.timepicker.min.js"></script>
    <script src="/js/scrollax.min.js"></script>
    <script src="/js/main.js?v=1.1"></script>
    @yield('js')
</body>
</html>