@extends('layouts.app')

@section('content')
    <section class="hero-wrap js-fullheight" style="background-image: url(images/main.jpg);" data-stellar-background-ratio="0.7">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight justify-content-center align-items-center">
                <div class="col-lg-12 ftco-animate d-flex align-items-center">
                    <div class="text text-center mt-5">
                        <span class="subheading">Центр дентальной имплантации</span>
                        <h1 class="mb-4">Clinic Dr. Vovk</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="services-section ftco-section">
        <div class="container" style="max-width:1500px;">
            <div class="row justify-content-center pb-3">
                <div class="col-md-10 heading-section text-center ftco-animate">
                    <h2 class="mb-4">Наши услуги</h2>
                </div>
            </div>
            @include('partials.services', ['services.index' => $services])
        </div>
    </section>

    <section class="ftco-section ftco-team">
        <div class="container-fluid px-md-5">
            <div class="row justify-content-center pb-3">
                <div class="col-md-10 heading-section text-center ftco-animate">
                    <h2 class="mb-4">Команда</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 ftco-animate">
                    @include('partials.team', ['team' => $team])
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-no-pt ftco-no-pb">
        <div class="container">
            <div class="row no-gutters justify-content-center mb-1 pb-2">
                <div class="col-md-6 text-center heading-section ftco-animate">
                    <h2 class="mb-4">Наша галерея</h2>
                </div>
            </div>
        </div>
        <div class="container-fluid p-0">
            <div class="ftco-team carousel-gallery owl-carousel">
                @include('partials.gallery', ['gallery' => $gallery])
            </div>
        </div>
    </section>
@endsection