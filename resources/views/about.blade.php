@extends('layouts.app')

@section('content')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/about.jpg');background-position:0 38%;">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
                <div class="col-md-9 ftco-animate pb-5 text-center">
                    <h2 class="mb-0 bread">О клинике</h2>
                    <p class="breadcrumbs"><span class="mr-2"><a href="/">Главная <i class="ion-ios-arrow-round-forward"></i></a></span> <span>О клинике</span></p>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-no-pb ftco-no-pt my-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 d-flex">
                    <div class="p-md-5 img about-img img-2 d-flex justify-content-center align-items-center" style="background-image: url(storage/images/gallery/SEL_0192.jpg);">
                        <a href="https://vimeo.com/531224371" class="icon popup-vimeo d-flex justify-content-center align-items-center">
                            <span class="icon-play"></span>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 py-md-5 pb-5 wrap-about pb-md-5 ftco-animate">
                    <div class="heading-section mb-4 mt-md-5">
                        <span class="subheading">О компании</span>
                        <h2 class="mb-4">Добро пожаловать в клинику Dr.Vovk</h2>
                    </div>
                    <div class="pb-md-5">
                        <p class="text-justify">16 августа 2010 года был открыт Центр дентальной имплантации «Clinic Dr. Vovk», который является результатом многолетнего труда известного имплантолога Вовк Виктора Евгеньевича. Сюда приезжают лечиться не только из Казахстана, но и из Киргизии, России, стран Европы и Америки. Среди пациентов встречаются иностранные дипломаты, известные предприниматели и деятели культуры.</p>
                        <p class="text-justify">Мы, как впрочем, и все человечество, осознаем важность заботы о своей внешности. И стоит ли говорить, что важной частью внешнего вида является красивая улыбка.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection