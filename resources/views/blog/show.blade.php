@extends('layouts.app')

@section('style')
    <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=606701d7b655eb0012793992&product=inline-reaction-buttons" async="async"></script>
@endsection

@section('content')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('/images/surgery.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
                <div class="col-md-9 ftco-animate pb-5 text-center">
                    <h1 class="mb-0 bread">{{ $post->title }}</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a href="/">Главная <i class="ion-ios-arrow-round-forward"></i></a></span> <span class="mr-2"><a href="/blog">Статьи <i class="ion-ios-arrow-forward"></i></a></span> <span>{{ $post->title }}</span></p>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row align-content-center justify-content-center">
                <div class="col-lg-9 ftco-animate">
                    <div class="post-content">
                        <div class="text-primary font-weight-normal">Время на чтение ~ {{ $post->ttr }} мин.</div>
                        {!! $post->content !!}
                    </div>
                    <hr>
                    <div class="sharethis-inline-reaction-buttons"></div>
                </div>
            </div>
        </div>
    </section>
@endsection
