@extends('layouts.app')

@section('content')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/blog.jpg');" data-stellar-background-ratio="0.9">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
                <div class="col-md-9 ftco-animate pb-5 text-center">
                    <h2 class="mb-0 bread">Блог клиники</h2>
                    <p class="breadcrumbs"><span class="mr-2"><a href="/">Главная <i class="ion-ios-arrow-round-forward"></i></a></span> <span>Блог</span></p>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ftco-animate">
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-md-12 d-flex ftco-animate mb-4">
                                <div class="blog-entry align-self-stretch d-md-flex w-100">
                                    <a href="/blog/{{ $post->slug }}" class="block-20" style="background-image:url('/storage/{{ $post->img }}');"></a>
                                    <div class="text d-block pl-md-4">
                                        <h3 class="heading"><a href="/blog/{{ $post->slug }}">{{ $post->title }}</a></h3>
                                        <p><a href="/blog/{{ $post->slug }}" class="btn btn-primary py-2 px-3">Читать статью</a></p>
                                        <div class="post-ttr">Время на чтение ~ {{ $post->ttr }} мин.</div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {{ $posts->links('vendor.pagination.default') }}
                </div> <!-- .col-md-8 -->
                <div class="col-lg-4 sidebar ftco-animate">
                    @search

                    <div class="sidebar-box ftco-animate" style="background-color:#f4f4f4;">
                        <h3 class="heading-2">Наши услуги</h3>
                        @foreach ($services as $service)
                            <div class="block-21 mb-4 d-flex">
                                <a class="blog-img mr-4" style="background-image: url('/storage/{{ $service->img }}');"></a>
                                <div class="text">
                                    <h3 class="heading-1"><a href="/services/{{ $service->slug }}">{{ $service->title }}</a></h3>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </section> <!-- .section -->
@endsection