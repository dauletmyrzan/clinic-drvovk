@extends('layouts.app')

@section('style')
    <style>
        .block-23.not-footer ul li span {
            color: #222!important;
            font-size: 14pt;
        }
    </style>
@endsection

@section('content')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/contact.jpg');" data-stellar-background-ratio="0.9">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
                <div class="col-md-9 ftco-animate pb-5 text-center">
                    <h2 class="mb-0 bread">Контакты</h2>
                    <p class="breadcrumbs"><span class="mr-2"><a href="/">Главная <i class="ion-ios-arrow-round-forward"></i></a></span> <span>Контакты</span></p>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 ftco-animate">
                    <div class="block-23 not-footer mb-3">
                        <ul>
                            <li><span class="text">Мы находимся на @address</span></li>
                            <li><span class="icon icon-phone"></span><span class="text">@phone</span></li>
                            <li><a href="mailto:@email"><span class="icon icon-envelope"></span><span class="text">@email</span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-7 ftco-animate" id="map_2gis">
                    <a class="dg-widget-link" href="http://2gis.kz/almaty/firm/70000001046758575/center/76.913826,43.246314/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Алматы</a>

                    <script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>

                    <script charset="utf-8">
                        new DGWidgetLoader({"width":"100%","height":600,"borderColor":"#eee","pos":{"lat":43.246314,"lon":76.913826,"zoom":16},"opt":{"city":"almaty"},"org":[{"id":"70000001046758575"}]});
                    </script>

                    <noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
                </div>
           </div>
        </div>
    </section> <!-- .section -->
@endsection