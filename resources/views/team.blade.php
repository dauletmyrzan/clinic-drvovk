@extends('layouts.app')

@section('content')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/team.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
                <div class="col-md-9 ftco-animate pb-5 text-center">
                    <h2 class="mb-0 bread">Наша команда</h2>
                    <p class="breadcrumbs">
                        <span class="mr-2">
                            <a href="/">Главная <i class="ion-ios-arrow-round-forward"></i></a>
                        </span> <span>Наша команда</span></p>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-team ftco-animate">
        <div class="container-fluid">
            <div class="row">
                @foreach ($team as $user)
                    <div class="col-xxl-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 mb-5 team-item">
                        <a href="javascript:void(0);" class="team text-center">
                            <div class="img" style="background-image: url('/storage/{{ $user->img }}');"></div>
                            <h2>{{ $user->name }}</h2>
                            <span class="position">{{ $user->title }}</span>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section> <!-- .section -->
@endsection