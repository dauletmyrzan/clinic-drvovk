<?php

use Butschster\Head\MetaTags\Viewport;

return [
    /*
     * Meta title section
     */
    'title' => [
        'default' => 'Clinic Dr.Vovk - Стоматологическая клиника в Алматы, drvovk.clinic',
        'separator' => '-',
        'max_length' => 255,
    ],


    /*
     * Meta description section
     */
    'description' => [
        'default' => 'Центр дентальной имплантации Clinic Dr. Vovk является результатом многолетнего труда известного имплантолога Вовк Виктора Евгеньевича.',
        'max_length' => 255,
    ],


    /*
     * Meta keywords section
     */
    'keywords' => [
        'default' => 'dr.vovk, clinic dr.vovk, vovk, clinic, стоматология, клиника доктора Вовка, стоматология, импланты, имплантация',
        'max_length' => 255
    ],

    /*
     * Default packages
     *
     * Packages, that should be included everywhere
     */
    'packages' => [
        // 'jquery', 'bootstrap', ...
    ],

    'charset' => 'utf-8',
    'robots' => null,
    'viewport' => Viewport::RESPONSIVE,
    'csrf_token' => true,
];